<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    public function index()
    {
        $rooms = Room::get();
        return view('front.rooms.index', compact('rooms'));
    }


    public function create($id)
    {
        $room = Room::find($id);
        return view('front.rooms.book', compact('room'));
    }

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'room_id' => 'required|exists:rooms,id',
                'date' => 'required|date',
                'from' => 'required',
                'to' => 'required'
            ],
            [
                'room.required' => 'Sorry,Room is required',
                'room.exists' => 'Sorry,Room is invalid',
                'date.required' => 'Date is required',
                'from.required' => 'From is required',
                'to.required' => 'To is required'
            ]
        );

        $check = Booking::where('room_id',$request->room_id)
            ->where( function($q) use($request)
            {
                $q->where('date' ,$request->date);
                $q->where('from', '>=' ,$request->from);
                $q->where('to', '<=' ,$request->to);
            })->get();


        if($check->count()) return back()->with('error','Sorry, room isn\'t available at this time');

        Booking::create
        (
            [
                'client_id' => Auth::guard('client')->user()->id,
                'room_id' => $request->room_id,
                'date' => $request->date,
                'from' => $request->from,
                'to' => $request->to
            ]
        );

        return redirect('/my_bookings')->with('success','Room booked successfully');
    }


    public function my_bookings()
    {
        return view('front.my_bookings');
    }


    public function show($id)
    {
        $room = Room::find($id);
        return view('front.rooms.show', compact('room'));
    }

}
