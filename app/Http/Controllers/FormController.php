<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class FormController extends Controller
{


    public function GetClientRegister()
    {
        return view('front.client.reg-page');
    }

    public function ClientRegister(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'password' => 'required|confirmed',
        ]);
            $request->merge(['password' => bcrypt($request->password)]);
            $client = Client::create($request->all());
        return view('front.client.login-page');
    }

    public function GetClientLogin()
    {
        return view('front.client.login-page');
    }

    public function ClientLogin(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required',
            'password' => 'required',
        ]);
        $client = Client::where('email', $request->input('email'))->first();
        if ($client) {
            if (Auth::guard('client')->attempt($request->only('email' ,'password'))) {
                return redirect('/home-client');
            }
            flash()->error('لا يوجد حساب مرتبط بهذا الرقم');
            return back();
        }
        return back();
    }

    public function home()
    {
        return view('front.client.home_client');
    }


    public function ClientLogout(Request $request){
        Auth()->guard('client')->logout();
        return redirect('/form');
    }
}
