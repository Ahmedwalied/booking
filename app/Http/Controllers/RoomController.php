<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Room;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;

class RoomController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::paginate(20);
        return view('admin.rooms',compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add-room');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'building' => 'required|numeric',
            'floor' => 'required|numeric',
            'image' => 'image|nullable|max:1999'
        ]);
        if (request('image')){
            $imagePath = request('image')->store('uploads' , 'public');
            $image = Image::make(public_path("storage/{$imagePath}"))
                ->fit(1200,1200);
            $image->save();
        }else{
            $imagePath = 'noimage.png';
        }
        $post = auth()->guard('web')->user()->rooms()->create([
            'name' => $request['name'],
            'building' => $request['building'],
            'floor' => $request['floor'],
            'image' => $imagePath
        ]);
        flash('Room Created')->success();
        return redirect('/all-rooms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = Room::findOrFail($id);
        return view('admin.edit-room',compact('room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->validate([
            'name' => 'required',
            'floor' => 'required',
            'building' => 'required',
            'image' => 'image',
        ]);

        if (request('image')){
            $imagePath = request('image')->store('uploads' , 'public');
            $image = Image::make(public_path("storage/{$imagePath}"))
                ->fit(1200,1200);
            $image->save();

            $imageArray = ['image' => $imagePath];
        }
        $room = Room::with('user')
            ->findOrFail($id)->update(array_merge(
                $data,
                $imageArray ?? []
            ));
        return redirect("/all-rooms");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Room::findOrFail($id);
        $record->delete();
        $booking = Booking::where('room_id',$record->id);
        $booking->delete();
        flash('Deleted!')->error();
        return back();
    }
}
