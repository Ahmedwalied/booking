<?php

namespace App\Providers;

use App\Models\Booking;
use App\Models\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        $bookings = Booking::where('client_id',Auth::guard('client')->user()->id)->get();
//        View::share(['global_bookings' => $bookings]);
        View::share(['all_rooms' => Room::get()]);

        Schema::defaultStringLength(191);
    }
}
