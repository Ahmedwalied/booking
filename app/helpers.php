<?php

use \Illuminate\Support\Facades\Auth;

function client()
{
    return Auth::guard('client')->user();
}

?>
