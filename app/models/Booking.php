<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Booking extends model
{
    use Notifiable;

    protected $fillable = [
        'client_id','room_id','date','from','to',
    ];

    public function rooms()
    {
        return $this->belongsToMany('App\Models\Room');
    }


    public function room()
    {
        return $this->belongsTo(Room::class,'room_id');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

}
