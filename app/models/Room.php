<?php

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Room extends model {

	protected $table = 'rooms';
	public $timestamps = true;
	protected $fillable = array('name', 'building', 'floor','image','available');


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class,'room_id');
    }

    public function roomImage()
    {
        $imagePath = ($this->image) ?  $this->image : '/uploads/noimage.png';
        return  '/storage/' . $imagePath;
    }

}
