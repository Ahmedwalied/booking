<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration {

	public function up()
	{
		Schema::create('front', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('client_id');
			$table->integer('room_id');
			$table->date('date');
			$table->time('from');
			$table->time('to');
		});
	}

	public function down()
	{
		Schema::drop('front');
	}
}
