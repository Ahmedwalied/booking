<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration {

	public function up()
	{
		Schema::create('rooms', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 255);
			$table->string('structure', 255);
			$table->string('floor', 255);
			$table->string('image',255);
			$table->integer('user_id');
		});
	}

	public function down()
	{
		Schema::drop('rooms');
	}
}
