@extends('layouts.app')

@section('content')
    <a href="/all-rooms" class="btn btn-outline-dark mb-2 mt-2">Back</a>
    <div class="card card-body bg-white text-center">
        {!! Form::open(['action' => 'RoomController@store','method' => 'POST' , 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name','Name')}}
            {{Form::text('name','',['class' => 'form-control','placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('floor','Floor')}}
            {{Form::text('floor','',[ 'class' => 'form-control','placeholder' => 'Floor'])}}
        </div>
        <div class="form-group">
            {{Form::label('building','Building')}}
            {{Form::text('building','',[ 'class' => 'form-control','placeholder' => 'Building'])}}
        </div>
        <div class="form-group">
            {{Form::file('image')}}
        </div>
        {{Form::submit('submit',['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
    </div>
@endsection
