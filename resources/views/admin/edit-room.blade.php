@extends('layouts.app')

@section('content')
    <a href="/all-rooms" class="btn btn-outline-dark mb-2 mt-2">Back</a>
    <div class="card card-body bg-white text-center">
        {!! Form::open(['action' => ['RoomController@update' , $room->id],'method' => 'POST','enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {{Form::label('name','Name')}}
            {{Form::text('name',$room->name,['class' => 'form-control','placeholder' => 'Name'])}}
        </div>
        <div class="form-group">
            {{Form::label('floor','Floor')}}
            {{Form::text('floor',$room->floor,[ 'class' => 'form-control','placeholder' => 'Floor'])}}
        </div>
        <div class="form-group">
            {{Form::label('building','Building')}}
            {{Form::text('building',$room->building,[ 'class' => 'form-control','placeholder' => 'Building'])}}
        </div>
        <div class="form-group">
            {{Form::file('image')}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('submit',['class' => 'btn btn-primary'])}}
        {!! Form::close() !!}
    </div>
@endsection
