@extends('layouts.app')

@section('content')
<!-- Main content -->
<section class="content">
@include('flash::message')
<!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title mt-2">Rooms</h3>
            <a href="{{action('RoomController@create')}}" class="btn btn-outline-dark float-right m-2">Create Room</a>
        </div>
        <div class="box-body">
            @if(count([$rooms]))
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Room Name</th>
                            <th>Floor</th>
                            <th>Building</th>
                            <th>Image</th>
                            <th>Edit</th>
                            <th class="text-center">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rooms as $room)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$room->name}}</td>
                                <td>{{$room->floor}}</td>
                                <td>{{$room->building}}</td>
                                <td>
                                    <img src="{{($room->roomImage())}}" alt="" style="height: 100px;">
                                </td>
                                <td class="text-center">
                                    <a href="{{url(route('all-rooms.edit', $room->id))}}" class=" btn btn-success btn-xs">
                                        <i class="fa fa-edit">Edit</i>
                                    </a>
                                </td>
                                <td class="text-center">
                                    {!! Form::open([
                                        'action' => ['RoomController@destroy',$room->id],
                                        'method' => 'delete'
                                    ]) !!}
                                    <button type="submit" class="delete_link btn btn-danger btn-xs">
                                        <i class="fa fa-trash-o">
                                            Delete
                                        </i></button>

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else

                <div class="alert alert-danger" role="alert">
                    No Data
                </div>
            @endif
        </div>
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->
@endsection
