@extends('layouts.app')

@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div>
        <form class="form-horizontal" method="POST" action="/client-register">
            @csrf
            <fieldset>
                <!-- Form Name -->
                <legend>Register</legend>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="name">Name</label>
                    <div class="col-md-4">
                        <input id="name" name="name" type="text" placeholder="Name" class="form-control input-md" required="">
                    </div>
                </div>
                <hr>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="email">Email</label>
                    <div class="col-md-4">
                        <input id="email" name="email" type="text" placeholder="Email address" class="form-control input-md" required="">
                    </div>
                </div>
                <hr>
                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="password">Password </label>
                    <div class="col-md-4">
                        <input id="password" name="password" type="password" placeholder="Password " class="form-control input-md" required="">
                    </div>
                </div>
                <hr>
                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="rpassword">Confirm</label>
                    <div class="col-md-4">
                        <input id="rpassword" name="rpassword" type="password" placeholder="Password Confirm" class="form-control input-md" required="">
                    </div>
                </div>
                <hr>

                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="save"></label>
                    <div class="col-md-8">
                        <button id="save" name="save" class="btn btn-success">Save</button>
                        <button id="cancel" name="cancel" class="btn btn-danger">Cancel</button>
                    </div>
                </div>

            </fieldset>
        </form>
    </div>
</div>
@endsection
