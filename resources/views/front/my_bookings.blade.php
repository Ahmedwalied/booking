@extends('layouts.app')

@section('content')
    <h1 style="text-align: center;">My Bookings</h1>
    <div class="card card-body bg-white text-center">
        @forelse(client()->bookings as $key => $booking)
            <div class="form-group row">
                <h2 style="text-align: center; display: block; margin: 0 auto;">{{$booking->room->name}}</h2>
                <span class="col-12">Date : {{$booking->date}}</span><hr/>
                <span class="col-12">From : {{$booking->from}} | To : {{$booking->to}}</span>
                <span class="col-12">Booked since : {{$booking->created_at->diffForHumans()}} </span>
            </div>
            @if($loop->last == $booking[$key])
                <hr>
            @endif
        @empty
            <h1>No Bookings</h1>
        @endforelse
    </div>
@endsection
