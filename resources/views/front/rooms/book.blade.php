@extends('layouts.app')

@section('content')
    <h1 style="text-align: center;">{{$room->name}}</h1>
    <div class="form-group border pt-3">
        <form class="form-horizontal form-group" method="post" action="/room/book">
            {{csrf_field()}}
            <div class="form-group col-12 row">
                <label class="col-3">Date</label>
                <input class="col-9" type="date" min="{{\Carbon\Carbon::now()->toDateString()}}" name="date" value="{{old('date')}}">
            </div>

            <div class="form-group col-12 row">
                <label class="col-3">From</label>
                <input class="col-9" type="time" name="from" value="{{old('from')}}">
            </div>

            <div class="form-group col-12 row">
                <label class="col-3">To</label>
                <input class="col-9" type="time" name="to" value="{{old('to')}}">
            </div>
            <input type="hidden" name="room_id" value="{{$room->id}}">
            <button class="col-6 btn btn-success m-auto d-block" style="text-align: center;" type="submit">Book</button>
        </form>
    </div>
@endsection
