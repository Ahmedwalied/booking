@extends('layouts.app')

@section('content')
    <div class="container">
    <h1 style="text-align: center;">Rooms</h1>
    <div class="card card-body bg-white text-center">
        <div class="form-group col-md-12">
            @foreach($rooms as $room)
                <div class="row" style="padding-bottom: 5px">
                    <img class="col-md-2" src="{{($room->roomImage())}}" alt="" style="height: 150px;">
                    <div class="float-right m-auto">
                        <h2>{{$room->name}}</h2>
                        <span>Building : {{$room->structure}}</span><br/>
                        <span>Floor : {{$room->floor}}</span><br/>
                        <a href="/room/{{$room->id}}/view"><button class="btn btn-info">Show</button></a>
                    </div>
                </div>
                <hr>
            @endforeach
        </div>
    </div>
    </div>
@endsection
