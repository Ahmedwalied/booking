@extends('layouts.app')

@section('content')
    <h1 style="text-align: center;">{{$room->name}}</h1>
    <a href="/room/{{$room->id}}/book" style="text-align: center; margin: 0 auto; display: block; padding-bottom: 5px;"><button class="btn btn-lg btn-info">Book Now</button> </a>
    <div class="card card-body bg-white text-center">
        @forelse($room->bookings as $key => $booking)
            <div class="form-group row">
                <span class="col-12">Date : {{$booking->date}}</span><hr/>
                <span class="col-12">From : {{$booking->from}} | To : {{$booking->to}}</span>
            </div>
            @if($loop->last == $booking[$key])
                <hr>
            @endif
        @empty
            <h1>No Bookings</h1>
        @endforelse
    </div>
@endsection
