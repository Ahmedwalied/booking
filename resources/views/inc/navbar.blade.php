<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <div class="container">
        @guest
        <a class="navbar-brand" href="/main">
            {{ config('app.name', 'Laravel') }}
        </a>
        @elseif(\Illuminate\Support\Facades\Auth::guard('client'))
            <a class="navbar-brand" href="/home-client">
            {{ config('app.name', 'Laravel') }}
            </a>
        @elseif(\Illuminate\Support\Facades\Auth::guard('web'))
            <a class="navbar-brand" href="/home">
                {{ config('app.name', 'Laravel') }}
            </a>
        @endguest
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            </ul>
            @guest
            @else
                @if(auth('web')->user())
                    <ul class="nav navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="/home">Home</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/all-rooms">Rooms</a>
                        </li>
                    </ul>
                @elseif(\Illuminate\Support\Facades\Auth::guard('client'))
                    <ul class="nav navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="/home-client">home</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/my_bookings">My Bookings</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="/rooms">Rooms</a>
                        </li>
                    </ul>
                @endif
            @endguest

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('/form')}}">Register/Login</a>
                    </li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        @if(auth('web')->user())
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @elseif(\Illuminate\Support\Facades\Auth::guard('client'))
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/client-logout') }}"
                                   onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                    {{ __('logout') }}
                                </a>

                                <form id="logout-form" action="{{ url('/client-logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @endif
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

