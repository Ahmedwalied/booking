@extends('layouts.app')

@section('content')
    <div class="container row d-flex justify-content-xl-center text-center m-3">

        <div class="card mr-5 " style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Login</h5>
                <hr>
                <a href="{{ route('login') }}" class="btn btn-primary mb-3">Login As Admin</a>
                <a href="{{url('/get-client-login')}}" class="btn btn-primary">Login As Client</a>
            </div>
        </div>
        <div class="card " style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">Register</h5>
                <hr>
                <a href="{{ route('register') }}" class="btn btn-primary mb-3">Register As Admin</a>
                <a href="{{url('/get-client-register')}}" class="btn btn-primary">Register As Client</a>
            </div>
        </div>
     </div>
@endsection
