<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/form', 'ClientController@form');
Route::get('/get-client-register', 'FormController@GetClientRegister');
Route::get('/get-client-login', 'FormController@GetClientLogin');
Route::post('/client-register', 'FormController@ClientRegister');
Route::post('/client-login', 'FormController@ClientLogin');

Route::group(['middleware' => 'auth:web'],function(){
    Route::resource('/all-rooms', 'RoomController');
});

Route::group(['middleware' => 'auth:client'],function(){
    Route::get('/home-client', 'FormController@home');
    Route::post('/client-logout', 'FormController@ClientLogout');
    Route::get('/rooms', 'BookingController@index');
    Route::post('/room/book', 'BookingController@store');
    Route::get('/my_bookings', 'BookingController@my_bookings');
    Route::get('/room/{id}/view', 'BookingController@show');
    Route::get('/room/{id}/book', 'BookingController@create');
});


